﻿using ForecastAlena.Services.Contracts;
using ForecastCortana.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Serilog;
using System;
using System.Net.NetworkInformation;

namespace Forecast.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly IAlenaService _alenaService;
        private readonly ICortanaService _cortanaService;

        public HomeController(IAlenaService alenaService, ICortanaService cortanaService)
        {
            this._alenaService = alenaService;
            this._cortanaService = cortanaService;
        }

        [HttpGet("")]
        //[Route("today")]
        public async Task<IActionResult> GetToday(string today)
        {
            // ADJUST second db connection
            try
            {
                Log.Debug("Alena GET started.");
                Log.Debug("Alena GET input ", today);

                var forecast = await _alenaService.Get();

                Log.Debug("Alena GET output", forecast);

                return Ok(forecast);
            }
            catch (Exception) // argumentnullexception
            {
                Log.Error("Alena GET failed. Status code: 500");
                return StatusCode(500);
            }

        }

        private bool ServerStatusBy(string url)
        {
            Ping pingSender = new Ping();
            PingReply reply = pingSender.Send(url);
            Console.WriteLine("Status of Host: {0}", url);

            if (reply.Status == IPStatus.Success)
            {
                Console.WriteLine("IP Address: {0}", reply.Address.ToString());
                return true;
            }
            else
                return false;
        }
    }
}
