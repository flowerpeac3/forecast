using ForecastAlena.Database;
using ForecastAlena.Database.Contracts;
using ForecastAlena.Services;
using ForecastAlena.Services.Contracts;
using ForecastCortana.Database;
using ForecastCortana.Database.Contracts;
using ForecastCortana.Services;
using ForecastCortana.Services.Contracts;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Serilog;

namespace Forecast.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(Configuration)
                .CreateLogger();
        }

        public IConfiguration Configuration { get; }

        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<AlenaDBContext>(options =>
                    options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"),
                    ef => ef.MigrationsAssembly(typeof(AlenaDBContext).Assembly.FullName)));
            services.AddScoped<IAlenaDBContext>(provider => provider.GetService<AlenaDBContext>());
            
            services.AddDbContext<CortanaDBContext>(options =>
                    options.UseSqlServer(Configuration.GetConnectionString("AlternativeConnection"),
                    ef => ef.MigrationsAssembly(typeof(CortanaDBContext).Assembly.FullName)));
            services.AddScoped<ICortanaDBContext>(provider => provider.GetService<CortanaDBContext>());

            services.AddTransient<IAlenaService, AlenaService>();
            services.AddTransient<ICortanaService, CortanaService>();

            services.AddHealthChecks()
                .AddDbContextCheck<AlenaDBContext>();

            services.AddHealthChecks()
                .AddDbContextCheck<CortanaDBContext>();

            services.AddApiVersioning(apiVerConfig =>
            {
                apiVerConfig.AssumeDefaultVersionWhenUnspecified = true;
                apiVerConfig.DefaultApiVersion = new ApiVersion(new DateTime(2022, 1, 22));
            });

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1a&b", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Title = "ForecastService - Web API",
                    Version = "v2a&b",
                    Description = "test service"
                });
            });

            services.AddControllers();
        }

        
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseHealthChecks("/checkhealth");

            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1a/swagger.json", "Alena API v1a");
            });

        }
    }
}
