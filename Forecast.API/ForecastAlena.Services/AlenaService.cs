﻿using ForecastAlena.Database.Contracts;
using ForecastAlena.Database.Models;
using ForecastAlena.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ForecastAlena.Services
{
    public class AlenaService : IAlenaService
    {
        private readonly IAlenaDBContext _context;

        public AlenaService(IAlenaDBContext context)
        {
            this._context = context;
        }
        public async Task<Alena> Get()
        {
            var date = CurrentDate();

            var forecast = await this._context.Alenas
                .FirstOrDefaultAsync(a => a.Day.Equals(date));


            if (forecast == null)
            {
                var alena = new Alena();
                alena.Day = date;

                await this._context.Alenas.AddAsync(alena);
                await this._context.SaveChangesAsync();

                //throw new Exception("You ocurred an error!");
            }

            return forecast;
        }

        private int CurrentDate()
        {
            return DateTime.Now.DayOfYear;
        }
    }
}
