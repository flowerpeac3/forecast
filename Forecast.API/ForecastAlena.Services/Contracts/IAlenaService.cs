﻿using ForecastAlena.Database.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ForecastAlena.Services.Contracts
{
    public interface IAlenaService
    {
        Task<Alena> Get();
    }
}
