﻿using ForecastAlena.Database.Contracts;
using ForecastAlena.Database.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ForecastAlena.Database
{
    public class AlenaDBContext : DbContext, IAlenaDBContext
    {
        public AlenaDBContext(DbContextOptions<AlenaDBContext> options)
            : base(options)
        {

        }

        public DbSet<Alena> Alenas { get; set; }

        public async Task<int> SaveChangesAsync()
        {
            return await base.SaveChangesAsync(); // read about this
        }
    }
}
