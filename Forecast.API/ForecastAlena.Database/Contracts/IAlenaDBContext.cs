﻿using ForecastAlena.Database.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ForecastAlena.Database.Contracts
{
    public interface IAlenaDBContext
    {
        DbSet<Alena> Alenas { get; set; } // read about this
        Task<int> SaveChangesAsync();
    }
}
