﻿using Forecast.AlenaDatabase.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Forecast.AlenaDatabase.Contracts
{
    internal interface IAlenaDBContext
    {
        DbSet<Alena> Alena { get; set; } // read about this
        Task<int> SaveChangesAsyncOne();
    }
}
