﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Forecast.AlenaDatabase.Models
{
    internal class Alena
    {
        public int AlenaId { get; set; }
        public double Temperature { get; set; }
        public double Pressure { get; set; }
        public int? Day { get; set; }
    }
}
