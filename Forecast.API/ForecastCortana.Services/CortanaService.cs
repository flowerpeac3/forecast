﻿using ForecastCortana.Database.Contracts;
using ForecastCortana.Database.Models;
using ForecastCortana.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ForecastCortana.Services
{
    public class CortanaService : ICortanaService
    {
        private readonly ICortanaDBContext _context;

        public CortanaService(ICortanaDBContext context)
        {
            this._context = context;
        }
        public async Task<Cortana> Get()
        {
            var date = CurrentDate();

            var forecast = await this._context.Cortanas
                .FirstOrDefaultAsync(a => a.Day.Equals(date));


            if (forecast == null)
            {
                var alena = new Cortana();
                alena.Day = date;

                await this._context.Cortanas.AddAsync(alena);
                await this._context.SaveChangesAsync();

                //throw new Exception("You ocurred an error!");
            }

            return forecast;
        }

        private int CurrentDate()
        {
            return DateTime.Now.DayOfYear;
        }
    }
}
