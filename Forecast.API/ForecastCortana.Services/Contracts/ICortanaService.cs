﻿using ForecastCortana.Database.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ForecastCortana.Services.Contracts
{
    public interface ICortanaService
    {
        Task<Cortana> Get();
    }
}
