﻿using ForecastCortana.Database.Contracts;
using ForecastCortana.Database.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ForecastCortana.Database
{
    public class CortanaDBContext : DbContext, ICortanaDBContext
    {
        public CortanaDBContext(DbContextOptions<CortanaDBContext> options)
            : base(options)
        {

        }

        public DbSet<Cortana> Cortanas { get; set; }

        public async Task<int> SaveChangesAsync()
        {
            return await base.SaveChangesAsync(); // read about this
        }
    }
}
