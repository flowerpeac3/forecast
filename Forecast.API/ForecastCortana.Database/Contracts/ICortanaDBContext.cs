﻿using ForecastCortana.Database.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ForecastCortana.Database.Contracts
{
    public interface ICortanaDBContext
    {
        DbSet<Cortana> Cortanas { get; set; } // read about this
        Task<int> SaveChangesAsync();
    }
}
