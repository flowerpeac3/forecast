﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ForecastCortana.Database.Models
{
    public class Cortana
    {
        public int CortanaId { get; set; }
        public double Temperature { get; set; }
        public double Pressure { get; set; }
        public int? Day { get; set; }
    }
}
